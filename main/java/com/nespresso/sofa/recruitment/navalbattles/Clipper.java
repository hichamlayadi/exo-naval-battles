package com.nespresso.sofa.recruitment.navalbattles;

public class Clipper extends Ship{

	public Clipper(int displacement, int masts) {
		super(displacement, masts);
	}

	@Override
	public double speed() {
		return 0.8 * super.speed();
	}
}
