package com.nespresso.sofa.recruitment.navalbattles;

public class Ship {

	private int masts;
	private int displacement;
	private int canons;

	public Ship(int displacement, int masts) {
		this(displacement,masts,0);
	}
	
	public Ship(int displacement, int masts, int canons) {
		this.displacement = displacement;
		this.masts = masts;
		this.canons = canons;
	}

	public double speed(){
		return new SpeedCalculator().canons(canons).displacement(displacement).masts(masts).speed();
	}
	
}
