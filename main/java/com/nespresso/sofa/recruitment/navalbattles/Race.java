package com.nespresso.sofa.recruitment.navalbattles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.assertj.core.util.Lists;

public class Race {

	private Ship[] ships;

	private static final Comparator<Ship> speedComparator = new Comparator<Ship>() {

		@Override
		public int compare(Ship shipA, Ship shipB) {
			return Double.compare(shipA.speed(), shipB.speed());
		}

	};
	public Race(Ship... ships) {
		this.ships = ships;
	}

	public Ship winner() {
		ArrayList<Ship> allShips = Lists.newArrayList(ships);
		Collections.sort(allShips,speedComparator);
		return allShips.get(0);
	}
}
