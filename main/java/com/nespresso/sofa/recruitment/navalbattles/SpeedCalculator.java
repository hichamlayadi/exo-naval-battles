package com.nespresso.sofa.recruitment.navalbattles;

public class SpeedCalculator {

	private int masts;
	private int displacement;
	private int canons;
	private double coefficient;
	
	public SpeedCalculator masts(int masts) {
		this.masts = masts;
		return this;
	}
	public SpeedCalculator displacement(int displacement) {
		this.displacement = displacement;
		return this;
	}
	public SpeedCalculator canons(int canons) {
		this.canons = canons;
		return this;
	}
	
	public Double speed(){
		if(this.coefficient == 0)
			this.coefficient = 1;
		return coefficient * Double.valueOf(displacement)/masts *(1 + (0.05 * canons));
	}
	
	
}
